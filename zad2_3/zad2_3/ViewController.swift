//
//  ViewController.swift
//  zad2_3
//
//  Created by Bartosz Kucharski on 13.12.2016.
//  Copyright © 2016 Bartosz Kucharski. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  var images = [SKPhoto]()
  let imagePicker = UIImagePickerController()
  
  
  @IBAction func btnPressed(_ sender: Any) {
    imagePicker.sourceType = .camera
    present(imagePicker, animated: true, completion: nil)
  }
  

  
  @IBAction func galleryBtnPressed(_ sender: Any) {
    
    if images.count == 0 {
    let alert = UIAlertController(title: nil, message: "No photo in gallery", preferredStyle: UIAlertControllerStyle.alert)
    let okButton = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil)
    alert.addAction(okButton)
    present(alert, animated: true, completion: nil)
    }
    else {
      
      let browser = SKPhotoBrowser(photos: images)
      browser.initializePageIndex(0)
      present(browser, animated: true, completion: {})
    }
    
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imagePicker.delegate = self
    SKPhotoBrowserOptions.displayAction = false
    
    //let photo = SKPhoto.photoWithImageURL("https://placehold.jp/150x150.png")
   // photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
    
  }
  
  
  

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

extension ViewController {
  
  
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
      let image1 = SKPhoto.photoWithImage(image)
      images.append(image1)
      imagePicker.dismiss(animated: true, completion: nil)
      
      let browser = SKPhotoBrowser(photos: images)
      browser.initializePageIndex(0)
      present(browser, animated: true, completion: {})
    }
  }
}
